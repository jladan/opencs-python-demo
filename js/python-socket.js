var PythonSocket = function(url) {
    // inherit from WebSocket
    this.socket = new WebSocket(url);
};


PythonSocket.prototype.initialize = function () {
    var data = { type: "init" }
    var msg = JSON.stringify(data);
    this.debugLog(msg);
    this.socket.send(msg);
};

// TODO add handler for the response

PythonSocket.prototype.eval = function (code) {
    var data = {
            uniqueID: this.uniqueID,
            type: "eval",
            code: code
        };
    msg = JSON.stringify(data);
    this.debugLog(msg);
    this.socket.send(msg);
};

// TODO add handler for the response

PythonSocket.prototype.exec = function (code) {
    var data = {
            uniqueID: this.uniqueID,
            type: "exec",
            code: code
        };
    msg = JSON.stringify(data);
    this.debugLog(msg);
    this.socket.send(msg);
    this.output.innerHTML = ""
};

PythonSocket.prototype.sendInput = function (s) {
    var data = {
        uniqueID: this.uniqueID,
        type: 'input',
        content: s,
    };
    msg = JSON.stringify(data);
    this.debugLog(msg);
    this.socket.send(msg);
};


// TODO separate the message handler from this function
// also, organize the handlers (maybe in different functions?)
PythonSocket.prototype.setOutput = function (element) {
    this.output = element;
    var ps = this;
    this.socket.onmessage = function (event) {
        msg = JSON.parse(event.data);
        if (msg.type === 'out')
            element.appendChild(document.createTextNode(msg.content)) ;
        else if (msg.type === 'err') {
            var e = document.createElement('span');
            e.setAttribute('style','color:red');
            e.appendChild(document.createTextNode(msg.content));
            element.appendChild(e);
        }
        else if (msg.type === 'input-req') {
            var inputField = document.createElement('input');
            var currentFocus = document.activeElement;
            inputField.onkeypress = function (e) {
                if (e.keyCode ===13) {
                    inputField.setAttribute('disabled','disabled');
                    console.log(currentFocus)
                    currentFocus.focus()
                    ps.sendInput(inputField.value);
                }
            };
            inputField.className = 'user-input';
            element.appendChild(inputField);
            element.appendChild(document.createTextNode('\n'));
            inputField.focus();
        }
    };
};


PythonSocket.prototype.setDebug = function (element) {
    this.debugOutput = element;
    this.socket.addEventListener("message", function (event) {
        msg = event.data;
        element.innerHTML += "\nRECEIVED MESSAGE:\n" + msg;
    });

    this.socket.onopen = function (event) {
        element.innerHTML += "\nConnection established\n";
    };

    this.socket.onclose = function (event) {
        element.innerHTML += "\nConnection closed\n";
    };
};

PythonSocket.prototype.debugLog = function (msg) {
    if (this.debugOutput) 
        this.debugOutput.innerHTML += "\nSENT MESSAGE:\n" + msg;
};
