# Python in browser using websockets

This is a demonstration website for running arbitrary python code in the browser. The code execution is performed by sending the code through websockets to the server, which executes that code and sends the result back to the web browser.

There is currently no security, ability to interrupt, or fallback if errors occur.
