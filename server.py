import tornado.ioloop
import tornado.web as web
import tornado.websocket as ws

import queue

import tornado.gen as gen
from concurrent.futures import ThreadPoolExecutor

thread_pool = ThreadPoolExecutor(4)

import multiprocessing as mp
import json

port = 8000
INPUT_TIMEOUT = 100

# use the interactive interpreter for tracebacks and syntax errors
from code import InteractiveInterpreter
ii = InteractiveInterpreter()

class InStream():
    """ An implementation of a stream that sends a request and waits to hear back
    """
    def __init__(self, send_request):
        self.send = send_request
        self._queue = mp.Queue()

    def readline(self):
        if self._queue.empty():
            self.send()
        # If the \n isn't trainling an empty string, an EOF error will be raised
        return self._queue.get(True, INPUT_TIMEOUT) + '\n'

    def add_input(self, s):
        self._queue.put(s)

    def clear_input(self):
        while not self._queue.empty():
            self._queue.get_nowait()


class ExecStream():
    """ A wrapper class for making connections have StringIO capability

    - The messages are wrapped in json so that they can be used directly in the browser
    """

    def __init__(self, conn, stream_label):
        self.conn=conn
        self.template = {
                'type': stream_label,
                }

    def write(self, s):
        result = self.template
        result.update({
                'content': s,
                })
        self.conn.write_message(json.dumps(result))

    def flush(self):
        self.outb = ""

import sys
class Capturing:
    """ Context class to capture stdout and stderr into streams
    """

    def __init__(self, outStream, inStream, errStream):
        self._out = outStream
        self._in = inStream
        self._err = errStream

    def __enter__(self):
        self._stdout = sys.stdout
        self._stdin = sys.stdin
        self._stderr = sys.stderr
        sys.stdout = self._out
        sys.stdin = self._in
        sys.stderr = self._err
        return self

    def __exit__(self, *args):
        sys.stderr = self._stderr
        sys.stdin = self._stdin
        sys.stdout = self._stdout

## Actual server code

class MainHandler(web.RequestHandler):
    def get(self):
        self.render("index.html", portNumber=port)

class DebugHandler(web.RequestHandler):
    def get(self):
        self.render("debug.html", portNumber=port)

class PythonWebSocket(ws.WebSocketHandler):
    def open(self):
        print("Python WebSocket opened")
        self._out = ExecStream(self, 'out')
        self._err = ExecStream(self, 'err')
        self._in = InStream(self.send_input_request)

    def on_message(self, message):
        data = json.loads(message)
        # TODO perform the eval properly
        if data['type'] == 'eval':
            self.write_message(data['code'] + "Evaluates to:\n    " + str(eval(data['code'])))
        elif data['type'] == 'exec':
            self.stream_code(data['code'])
        elif data['type'] == 'input':
            self._in.add_input(data['content'])
        else:
            self.write_message("Invalid message type")

    @gen.coroutine
    def stream_code(self, code):
        """ Helper function to handle running code and capturing output
        Output gets captured into a stream
        """
        try:
            print('clearing input')
            self._in.clear_input()
            print('cleared input')
        except (queue.Empty):
            self.send_error("error clearing the input queue")

        with Capturing(self._out, self._in, self._err):
            try:
                yield thread_pool.submit(exec, code,{})
                self.send_finished()
            except (SyntaxError):
                ii.showsyntaxerror()
                self.send_finished(True, 'There is a problem with the syntax')
            except (queue.Empty):
                self.send_error("timeout waiting for user input")
                self.send_finished(True, 'User did not give input in time')
            except:
                ii.showtraceback()
                self.send_finished(True, 'There was an error')

    def on_close(self):
        print("Python WebSocket Closed")

    def send_error(self, s):
        msg = {
                'type': 'error',
                'msg':  s,
                }
        self.write_message(json.dumps(msg))
    
    def send_finished(self, e=False, s='No errors detected'):
        msg = {
                'type': 'stop',
                'error': e,
                'msg': s,
                }
        self.write_message(json.dumps(msg))


    def send_input_request(self):
        msg = {
                'type': 'input-req',
                }
        self.write_message(json.dumps(msg))



application = web.Application([
    (r"/", MainHandler),
    (r"/debug", DebugHandler),
    (r"/js/(.*)", web.StaticFileHandler, {'path': 'js'}),
    (r"/css/(.*)", web.StaticFileHandler, {'path': 'css'}),
    (r"/python-socket", PythonWebSocket),
    ])

if __name__ == "__main__":
    if len(sys.argv) > 1:
        port = int(sys.argv[1])
    if len(sys.argv) > 2:
        INPUT_TIMEOUT = float(sys.argv[2])
    print("Opening server at http://localhost:%s" % port)
    application.listen(port)
    tornado.ioloop.IOLoop.instance().start()
